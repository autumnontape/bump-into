#![no_main]
use bump_into::BumpInto;
use libfuzzer_sys::arbitrary::{self, Arbitrary};
use libfuzzer_sys::fuzz_target;
use std::alloc::Layout;
use std::mem::{self, MaybeUninit};
use std::ptr::{self, NonNull};

const TOO_MANY_BYTES_ON_STACK: usize = 1024 * 1024;

const MAX_SIZE: usize = 1 << 25;
const MAX_ALIGN: usize = 1 << 24;

#[derive(Debug, Copy, Clone)]
struct InitialBumpInto {
    size: usize,
    align_high: usize,
    align_low: usize,
}

impl<'a> Arbitrary<'a> for InitialBumpInto {
    fn arbitrary(u: &mut arbitrary::Unstructured<'a>) -> arbitrary::Result<Self> {
        let bits = <(u32, u8, u8) as Arbitrary<'a>>::arbitrary(u)?;

        let size_input = bits.0 as usize;
        let align_high = 1usize << (bits.1 as usize % (MAX_ALIGN.trailing_zeros() as usize + 1));
        let align_low = 1usize << (bits.2 as usize % (MAX_ALIGN.trailing_zeros() as usize + 1));

        // if the top of the region of memory is at the highest
        // position that's aligned to `align_high`, the bottom
        // should be aligned to `align_low`.
        let correction = align_low
            // subtracting the highest `align_high`-aligned address
            // is the same as adding `align_high`.
            .wrapping_add(align_high)
            .wrapping_add(size_input)
            & (align_low * 2 - 1);
        let size = (size_input.wrapping_sub(correction)) & (MAX_SIZE - 1);

        Ok(InitialBumpInto {
            size,
            align_high,
            align_low,
        })
    }

    #[inline]
    fn size_hint(depth: usize) -> (usize, Option<usize>) {
        <(u32, u8, u8) as Arbitrary<'a>>::size_hint(depth)
    }
}

#[derive(Debug, Copy, Clone)]
struct ArbitraryLayout(Layout);

impl<'a> Arbitrary<'a> for ArbitraryLayout {
    fn arbitrary(u: &mut arbitrary::Unstructured<'a>) -> arbitrary::Result<Self> {
        let (size, align_bits) = <(usize, u8) as Arbitrary<'a>>::arbitrary(u)?;

        let align = 1 << (align_bits as usize % (usize::MAX.trailing_ones() as usize - 1));
        let size = match (usize::MAX - align).checked_add(2) {
            Some(x) => size % x,
            None => size,
        };

        Ok(Self(Layout::from_size_align(size, align).unwrap()))
    }

    #[inline]
    fn size_hint(depth: usize) -> (usize, Option<usize>) {
        <(usize, u8) as Arbitrary<'a>>::size_hint(depth)
    }
}

macro_rules! make_prefab_types {
    { $($name:ident: $size:expr, $align:literal;)* } => {
        trait Prefab: Sized {
            fn alloc_space_to_limit_for<'a>(bump_into: &'_ BumpInto<'a>) -> (NonNull<Self>, usize);
            fn alloc<'a>(bump_into: &'_ BumpInto<'a>, fill: u8) -> Option<&'a mut Self>;
            fn alloc_with<'a>(bump_into: &'_ BumpInto<'a>, fill: u8) -> Option<&'a mut Self>;
            fn alloc_copy<'a>(bump_into: &'_ BumpInto<'a>, fill: u8) -> Option<&'a mut Self>;
            fn alloc_copy_slice<'a>(bump_into: &'_ BumpInto<'a>, fill: &'_ [u8]) -> Option<&'a mut [Self]>;
            fn alloc_copy_concat_slices<'a>(bump_into: &'_ BumpInto<'a>, fill: Vec<&'_ [u8]>) -> Option<&'a mut [Self]>;
            fn alloc_n_with<'a>(bump_into: &'_ BumpInto<'a>, count: usize, fill: Vec<Option<u8>>) -> Option<&'a mut [Self]>;
            fn alloc_down_with<'a>(bump_into: &'_ mut BumpInto<'a>, fill: Vec<Option<u8>>) -> &'a mut [Self];
        }

        $(
            #[allow(non_camel_case_types)]
            #[derive(Debug, Copy, Clone, PartialEq, Eq)]
            #[repr(C, align($align))]
            struct $name([u8; $size]);

            impl Prefab for $name {
                fn alloc_space_to_limit_for<'a>(bump_into: &'_ BumpInto<'a>) -> (NonNull<Self>, usize) {
                    bump_into.alloc_space_to_limit_for::<Self>()
                }

                fn alloc<'a>(bump_into: &'_ BumpInto<'a>, fill: u8) -> Option<&'a mut Self> {
                    assert!($size < TOO_MANY_BYTES_ON_STACK);

                    match bump_into.alloc(Self([fill; $size])) {
                        Ok(result) => {
                            assert_eq!(result, &Self([fill; $size]));

                            Some(result)
                        }
                        Err(this) => {
                            assert_eq!(this, Self([fill; $size]));

                            None
                        }
                    }
                }

                fn alloc_with<'a>(bump_into: &'_ BumpInto<'a>, fill: u8) -> Option<&'a mut Self> {
                    assert!($size < TOO_MANY_BYTES_ON_STACK);

                    match bump_into.alloc_with(|| Self([fill; $size])) {
                        Ok(result) => {
                            assert_eq!(result, &Self([fill; $size]));

                            Some(result)
                        }
                        Err(f) => {
                            assert_eq!(f(), Self([fill; $size]));

                            None
                        }
                    }
                }

                fn alloc_copy<'a>(bump_into: &'_ BumpInto<'a>, fill: u8) -> Option<&'a mut Self> {
                    unsafe {
                        let manual_alloc = std::alloc::alloc(Layout::new::<Self>());
                        ptr::write_bytes(manual_alloc, fill, $size);

                        let result = bump_into.alloc_copy(&*(manual_alloc as *mut Self));

                        if let Some(result) = &result {
                            for b in result.0.iter() {
                                assert_eq!(*b, fill);
                            }
                        }

                        std::alloc::dealloc(manual_alloc, Layout::new::<Self>());

                        result
                    }
                }

                fn alloc_copy_slice<'a>(bump_into: &'_ BumpInto<'a>, fill: &'_ [u8]) -> Option<&'a mut [Self]> {
                    if bump_into.available_spaces_for::<Self>() < fill.len() {
                        return None;
                    }

                    let len = fill.len();
                    let mut input = Vec::<Self>::with_capacity(len);

                    unsafe {
                        let spare_capacity = input.spare_capacity_mut();

                        for (f, slot) in fill.iter().copied().zip(spare_capacity) {
                            ptr::write_bytes(MaybeUninit::<Self>::as_mut_ptr(slot) as *mut u8, f, $size);
                        }

                        input.set_len(len);
                    }

                    let result = bump_into.alloc_copy_slice(input.as_slice());

                    if let Some(result) = &result {
                        assert_eq!(result.len(), fill.len());

                        for (f, value) in fill.iter().zip(result.iter()) {
                            for b in value.0.iter() {
                                assert_eq!(*b, *f);
                            }
                        }
                    }

                    result
                }

                fn alloc_copy_concat_slices<'a>(bump_into: &'_ BumpInto<'a>, fill: Vec<&'_ [u8]>) -> Option<&'a mut [Self]> {
                    if bump_into.available_spaces_for::<Self>() < fill.iter().map(|v| v.len()).sum() {
                        return None;
                    }

                    let vec_vec = fill
                        .iter()
                        .map(|v| {
                            let len = v.len();
                            let mut vec = Vec::<Self>::with_capacity(len);

                            unsafe {
                                let spare_capacity = vec.spare_capacity_mut();

                                for (f, slot) in v.iter().copied().zip(spare_capacity) {
                                    ptr::write_bytes(
                                        MaybeUninit::<Self>::as_mut_ptr(slot) as *mut u8,
                                        f,
                                        $size,
                                    );
                                }

                                vec.set_len(len);
                            }

                            vec
                        })
                        .collect::<Vec<Vec<Self>>>();

                    let input = vec_vec.iter().map(|v| v.as_slice()).collect::<Vec<&[Self]>>();

                    let result = bump_into.alloc_copy_concat_slices(input.as_slice());

                    if let Some(result) = &result {
                        assert_eq!(result.len(), fill.iter().map(|s| s.len()).sum());

                        for (f, value) in fill.iter().flat_map(|s| s.iter()).zip(result.iter()) {
                            for b in value.0.iter() {
                                assert_eq!(*b, *f);
                            }
                        }
                    }

                    result
                }

                fn alloc_n_with<'a>(bump_into: &'_ BumpInto<'a>, mut count: usize, fill: Vec<Option<u8>>) -> Option<&'a mut [Self]> {
                    assert!($size < TOO_MANY_BYTES_ON_STACK);

                    if mem::size_of::<Self>() == 0
                        && count > bump_into.available_bytes() * 3
                        && !fill.iter().any(Option::is_none)
                    {
                        count = bump_into.available_bytes();
                    }

                    let mut next_was_called = false;
                    let mut fill_iter = std::iter::repeat(fill.as_slice()).flatten().copied();
                    let fill_iter = std::iter::from_fn(|| {
                        next_was_called = true;

                        fill_iter.next().flatten().map(|f| Self([f; $size]))
                    });

                    match bump_into.alloc_n_with(count, fill_iter) {
                        Ok(result) => {
                            if let Some(iter_len) = fill.iter().position(Option::is_none) {
                                // finite iterator
                                assert_eq!(result.len(), std::cmp::min(count, iter_len));

                                for (f, value) in fill.iter().zip(result.iter()) {
                                    for b in value.0.iter() {
                                        assert_eq!(*b, f.unwrap());
                                    }
                                }
                            } else {
                                // infinite iterator
                                assert_eq!(result.len(), count);

                                for (f, value) in std::iter::repeat(fill.as_slice())
                                    .flatten()
                                    .zip(result.iter())
                                {
                                    for b in value.0.iter() {
                                        assert_eq!(*b, f.unwrap());
                                    }
                                }
                            }

                            Some(result)
                        }
                        Err(_f) => {
                            assert!(!next_was_called);

                            None
                        }
                    }
                }

                fn alloc_down_with<'a>(bump_into: &'_ mut BumpInto<'a>, fill: Vec<Option<u8>>) -> &'a mut [Self] {
                    assert!($size < TOO_MANY_BYTES_ON_STACK);

                    if mem::size_of::<Self>() == 0 && !fill.iter().any(Option::is_none) {
                        return &mut [];
                    }

                    let available_spaces = bump_into.available_spaces_for::<Self>();

                    let mut next_was_called = false;
                    let mut fill_iter = std::iter::repeat(fill.as_slice()).flatten().copied();
                    let fill_iter = std::iter::from_fn(|| {
                        next_was_called = true;

                        fill_iter.next().flatten().map(|f| Self([f; $size]))
                    });

                    let result = bump_into.alloc_down_with(fill_iter);

                    if let Some(iter_len) = fill.iter().position(Option::is_none) {
                        // finite iterator
                        assert_eq!(result.len(), std::cmp::min(available_spaces, iter_len));

                        for (f, value) in fill.iter().zip(result.iter().rev()) {
                            for b in value.0.iter() {
                                assert_eq!(*b, f.unwrap());
                            }
                        }
                    } else {
                        // infinite iterator
                        assert_eq!(result.len(), available_spaces);

                        for (f, value) in std::iter::repeat(fill.as_slice())
                            .flatten()
                            .zip(result.iter().rev())
                        {
                            for b in value.0.iter() {
                                assert_eq!(*b, f.unwrap());
                            }
                        }
                    }

                    result
                }
            }
        )*

        #[allow(non_camel_case_types)]
        #[derive(Debug, Copy, Clone, Arbitrary)]
        enum PrefabType {
            $($name,)*
        }

        impl PrefabType {
            fn size(self) -> usize {
                match self {
                    $(
                        Self::$name => $size,
                    )*
                }
            }

            fn align(self) -> usize {
                match self {
                    $(
                        Self::$name => $align,
                    )*
                }
            }

            fn layout(self) -> Layout {
                let (size, align) = match self {
                    $(
                        Self::$name => ($size, $align),
                    )*
                };

                Layout::from_size_align(size, align).unwrap()
            }

            fn alloc_space_to_limit_for<'a>(self, bump_into: &'_ BumpInto<'a>) -> (NonNull<u8>, usize, usize) {
                match self {
                    $(
                        Self::$name => {
                            let (p, len) = $name::alloc_space_to_limit_for(bump_into);

                            if mem::size_of::<$name>() == $size {
                                (p.cast::<u8>(), len * $size, len)
                            } else {
                                (p.cast::<u8>(), $size, len)
                            }
                        }
                    )*
                }
            }

            fn alloc<'a>(self, bump_into: &'_ BumpInto<'a>, fill: u8) -> Option<&'a mut [u8]> {
                match self {
                    $(
                        Self::$name => $name::alloc(bump_into, fill).map(|p| &mut p.0[..]),
                    )*
                }
            }

            fn alloc_with<'a>(self, bump_into: &'_ BumpInto<'a>, fill: u8) -> Option<&'a mut [u8]> {
                match self {
                    $(
                        Self::$name => $name::alloc_with(bump_into, fill).map(|p| &mut p.0[..]),
                    )*
                }
            }

            fn alloc_copy<'a>(self, bump_into: &'_ BumpInto<'a>, fill: u8) -> Option<&'a mut [u8]> {
                match self {
                    $(
                        Self::$name => $name::alloc_copy(bump_into, fill).map(|p| &mut p.0[..]),
                    )*
                }
            }

            fn alloc_copy_slice<'a>(self, bump_into: &'_ BumpInto<'a>, fill: &'_ [u8]) -> Option<Vec<&'a mut [u8]>> {
                match self {
                    $(
                        Self::$name => {
                            $name::alloc_copy_slice(bump_into, fill)
                                .map(|v| {
                                    v.iter_mut().map(|p| &mut p.0[..]).collect()
                                })
                        }
                    )*
                }
            }

            fn alloc_copy_concat_slices<'a>(self, bump_into: &'_ BumpInto<'a>, fill: Vec<&'_ [u8]>) -> Option<Vec<&'a mut [u8]>> {
                match self {
                    $(
                        Self::$name => {
                            $name::alloc_copy_concat_slices(bump_into, fill)
                                .map(|v| {
                                    v.iter_mut().map(|p| &mut p.0[..]).collect()
                                })
                        }
                    )*
                }
            }

            fn alloc_n_with<'a>(self, bump_into: &'_ BumpInto<'a>, count: usize, fill: Vec<Option<u8>>) -> Option<Vec<&'a mut [u8]>> {
                match self {
                    $(
                        Self::$name => {
                            $name::alloc_n_with(bump_into, count, fill)
                                .map(|v| {
                                    v.iter_mut().map(|p| &mut p.0[..]).collect()
                                })
                        }
                    )*
                }
            }

            fn alloc_down_with<'a>(self, bump_into: &'_ mut BumpInto<'a>, fill: Vec<Option<u8>>) -> Vec<&'a mut [u8]> {
                match self {
                    $(
                        Self::$name => {
                            $name::alloc_down_with(bump_into, fill)
                                .iter_mut()
                                .map(|p| &mut p.0[..])
                                .collect()
                        }
                    )*
                }
            }
        }
    };
}

make_prefab_types! {
    Size1_Align1: 1, 1;
    Size2_Align2: 2, 2;
    Size4_Align4: 4, 4;
    Size8_Align8: 8, 8;
    Size16_Align16: 16, 16;
    Size1KiB_Align1KiB: 1024, 1024;
    Size4KiB_Align4KiB: 4 * 1024, 4096;
    Size1MiB_Align1MiB: 1048576, 1048576;

    Size2_Align1: 2, 1;
    Size4_Align2: 4, 2;
    Size8_Align4: 8, 4;
    Size16_Align8: 16, 8;
    Size32_Align16: 32, 16;
    Size2KiB_Align1KiB: 2 * 1024, 1024;
    Size8KiB_Align4KiB: 8 * 1024, 4096;
    Size2MiB_Align1MiB: 2 * 1048576, 1048576;

    Size3_Align1: 3, 1;
    Size6_Align2: 6, 2;
    Size12_Align4: 12, 4;
    Size24_Align8: 24, 8;
    Size48_Align16: 48, 16;
    Size3KiB_Align1KiB: 3 * 1024, 1024;
    Size12KiB_Align4KiB: 12 * 1024, 4096;
    Size3MiB_Align1MiB: 3 * 1048576, 1048576;

    Size8_Align1: 2, 1;
    Size16_Align2: 4, 2;
    Size32_Align4: 8, 4;
    Size64_Align8: 16, 8;
    Size128_Align16: 32, 16;
    Size8KiB_Align1KiB: 2 * 1024, 1024;
    Size32KiB_Align4KiB: 8 * 1024, 4096;
    Size8MiB_Align1MiB: 8 * 1048576, 1048576;

    Size101_Align1: 101, 1;
    Size202_Align2: 202, 2;
    Size404_Align4: 404, 4;
    Size808_Align8: 808, 8;
    Size1616_Align16: 1616, 16;
    Size101KiB_Align1KiB: 101 * 1024, 1024;
    Size404KiB_Align4KiB: 404 * 1024, 4096;
    Size101MiB_Align1MiB: 101 * 1048576, 1048576;

    Size2053_Align1: 2053, 1;
    Size4106_Align2: 4106, 2;
    Size8212_Align4: 8212, 4;
    Size16424_Align8: 16424, 8;
    Size32848_Align16: 32848, 16;

    Size0_Align1: 0, 1;
    Size0_Align2: 0, 2;
    Size0_Align4: 0, 4;
    Size0_Align8: 0, 8;
    Size0_Align16: 0, 16;
    Size0_Align1KiB: 0, 1024;
    Size0_Align4KiB: 0, 4096;
    Size0_Align1MiB: 0, 1048576;
}

#[derive(Debug, Clone, Arbitrary)]
enum Allocation<'a> {
    AllocSpace {
        layout: ArbitraryLayout,
    },
    AllocSpaceToLimitFor {
        ty: PrefabType,
    },
    Alloc {
        ty: PrefabType,
        fill: u8,
    },
    AllocWith {
        ty: PrefabType,
        fill: u8,
    },
    AllocCopy {
        ty: PrefabType,
        fill: u8,
    },
    AllocCopySlice {
        ty: PrefabType,
        fill: &'a [u8],
    },
    AllocCopyConcatSlices {
        ty: PrefabType,
        fill: Vec<&'a [u8]>,
    },
    AllocCopyConcatStrs {
        strs: Vec<&'a str>,
    },
    AllocNWith {
        ty: PrefabType,
        count: usize,
        fill: Vec<Option<u8>>,
        at_least_one: Option<u8>,
    },
    AllocDownWith {
        ty: PrefabType,
        fill: Vec<Option<u8>>,
        at_least_one: Option<u8>,
    },
}

fuzz_target!(|input: (InitialBumpInto, Vec<Allocation<'_>>)| {
    let (initial, allocations) = input;

    let mut space = Vec::<u8>::with_capacity(initial.size + MAX_SIZE);

    // ensure the end of the space is aligned precisely to `initial.align`,
    // neither less nor more
    let correction = initial
        .align_high
        .wrapping_neg()
        .wrapping_sub(space.as_ptr() as usize + initial.size)
        & (MAX_SIZE - 1);
    let space = &mut space.spare_capacity_mut()[correction..];
    let space = &mut space[..initial.size];

    assert_eq!(
        (space.as_ptr() as usize + space.len()) & (MAX_SIZE - 1),
        initial.align_high.wrapping_neg() & (MAX_SIZE - 1),
    );

    assert_eq!(
        space.as_ptr() as usize & (initial.align_low * 2 - 1),
        initial.align_low,
    );

    let mut bump_into = BumpInto::from_slice(space);

    // this could be tracked comprehensively, but
    // I didn't feel like making that happen.
    // it only tracks the results of alloc_space,
    // alloc_space_to_limit_for, & alloc_down_with.
    let mut last_successful_allocation = usize::MAX;

    for allocation in allocations {
        let starting_bytes = bump_into.available_bytes();

        match allocation {
            Allocation::AllocSpace { layout } => {
                let result = bump_into.alloc_space(layout.0);

                if !result.is_null() {
                    assert_eq!(result as usize & (layout.0.align() - 1), 0);
                    assert!(bump_into.available_bytes() <= starting_bytes - layout.0.size());

                    if layout.0.size() != 0 {
                        assert!(last_successful_allocation - result as usize >= layout.0.size());

                        last_successful_allocation = result as usize;
                    }
                }
            }
            Allocation::AllocSpaceToLimitFor { ty } => {
                let result = ty.alloc_space_to_limit_for(&bump_into);

                assert_ne!(result.0.as_ptr(), ptr::null_mut());

                if ty.size() == 0 {
                    assert_eq!(bump_into.available_bytes(), starting_bytes);
                    assert_eq!(result.1, 0);
                    assert_eq!(result.2, usize::MAX);
                } else {
                    assert_eq!(bump_into.available_spaces(ty.layout()), 0);
                    assert!(bump_into.available_bytes() < ty.size() + ty.align() - 1);

                    if result.2 > 0 {
                        assert!(
                            last_successful_allocation - result.0.as_ptr() as usize >= ty.size()
                        );

                        last_successful_allocation = result.0.as_ptr() as usize;
                    }
                }
            }
            Allocation::Alloc { ty, fill } => {
                if ty.size() >= TOO_MANY_BYTES_ON_STACK {
                    continue;
                }

                let starting_available_spaces = bump_into.available_spaces(ty.layout());

                let result = ty.alloc(&bump_into, fill);

                assert_eq!(result.is_some(), starting_available_spaces > 0);

                if result.is_some() {
                    if ty.size() != 0 {
                        assert_eq!(
                            bump_into.available_spaces(ty.layout()),
                            starting_available_spaces - 1
                        );
                    }

                    assert!(bump_into.available_bytes() <= starting_bytes - ty.size());
                }
            }
            Allocation::AllocWith { ty, fill } => {
                if ty.size() >= TOO_MANY_BYTES_ON_STACK {
                    continue;
                }

                let starting_available_spaces = bump_into.available_spaces(ty.layout());

                let result = ty.alloc_with(&bump_into, fill);

                assert_eq!(result.is_some(), starting_available_spaces > 0);

                if result.is_some() {
                    if ty.size() != 0 {
                        assert_eq!(
                            bump_into.available_spaces(ty.layout()),
                            starting_available_spaces - 1
                        );
                    }

                    assert!(bump_into.available_bytes() <= starting_bytes - ty.size());
                }
            }
            Allocation::AllocCopy { ty, fill } => {
                let starting_available_spaces = bump_into.available_spaces(ty.layout());

                let result = ty.alloc_copy(&bump_into, fill);

                assert_eq!(result.is_some(), starting_available_spaces > 0);

                if result.is_some() {
                    if ty.size() != 0 {
                        assert_eq!(
                            bump_into.available_spaces(ty.layout()),
                            starting_available_spaces - 1
                        );
                    }

                    assert!(bump_into.available_bytes() <= starting_bytes - ty.size());
                }
            }
            Allocation::AllocCopySlice { ty, fill } => {
                let starting_available_spaces = bump_into.available_spaces(ty.layout());
                let len = fill.len();

                let result = ty.alloc_copy_slice(&bump_into, fill);

                assert_eq!(result.is_some(), starting_available_spaces >= len);

                if result.is_some() {
                    if ty.size() != 0 {
                        assert_eq!(
                            bump_into.available_spaces(ty.layout()),
                            starting_available_spaces - len
                        );
                    }

                    assert!(bump_into.available_bytes() <= starting_bytes - ty.size() * len);
                }
            }
            Allocation::AllocCopyConcatSlices { ty, fill } => {
                let starting_available_spaces = bump_into.available_spaces(ty.layout());
                // the slices in `fill` won't overlap in memory,
                // so there's no risk they'll overflow a `usize`.
                let len = fill.iter().map(|v| v.len()).sum::<usize>();

                let result = ty.alloc_copy_concat_slices(&bump_into, fill);

                assert_eq!(result.is_some(), starting_available_spaces >= len);

                if result.is_some() {
                    if ty.size() != 0 {
                        assert_eq!(
                            bump_into.available_spaces(ty.layout()),
                            starting_available_spaces - len
                        );
                    }

                    assert!(bump_into.available_bytes() <= starting_bytes - ty.size() * len);
                }
            }
            Allocation::AllocCopyConcatStrs { strs } => {
                // see the comment on the `len` calculation for
                // the `AllocCopyConcatSlices` case.
                let len = strs.iter().map(|s| s.len()).sum::<usize>();

                let result = bump_into.alloc_copy_concat_strs(strs.as_slice());

                assert_eq!(result.is_some(), starting_bytes >= len);

                if let Some(result) = result {
                    assert_eq!(bump_into.available_bytes(), starting_bytes - len);
                    std::str::from_utf8(result.as_bytes()).unwrap();
                }
            }
            Allocation::AllocNWith {
                ty,
                count,
                mut fill,
                at_least_one,
            } => {
                if ty.size() >= TOO_MANY_BYTES_ON_STACK {
                    continue;
                }

                fill.push(at_least_one);

                let starting_available_spaces = bump_into.available_spaces(ty.layout());

                let result = ty.alloc_n_with(&bump_into, count, fill);

                assert_eq!(result.is_some(), starting_available_spaces >= count);

                if result.is_some() {
                    if ty.size() != 0 {
                        assert_eq!(
                            bump_into.available_spaces(ty.layout()),
                            starting_available_spaces - count
                        );
                    }

                    assert!(bump_into.available_bytes() <= starting_bytes - ty.size() * count);
                }
            }
            Allocation::AllocDownWith {
                ty,
                mut fill,
                at_least_one,
            } => {
                if ty.size() >= TOO_MANY_BYTES_ON_STACK {
                    continue;
                }

                fill.push(at_least_one);

                let starting_available_spaces = bump_into.available_spaces(ty.layout());

                let result = ty.alloc_down_with(&mut bump_into, fill);

                assert!(bump_into.available_bytes() <= starting_bytes - ty.size() * result.len());

                if ty.size() != 0 {
                    assert_eq!(
                        bump_into.available_spaces(ty.layout()),
                        starting_available_spaces - result.len()
                    );
                }

                if ty.size() != 0 && result.len() != 0 {
                    assert!(
                        last_successful_allocation - result[0].as_ptr() as usize
                            >= ty.size() * result.len()
                    );

                    last_successful_allocation = result[0].as_ptr() as usize;
                }
            }
        }
    }
});
